# Part 1
- Have everyone create tasks in the KanBan board "Open" swim lane
    - Everyone can look at what's currently on the board, and contribute additional Tasks to be done
    - Format for tasks:
        - Title, Definition of Done, Due Date, Time Estimate (can be done via comment)
- On Monday, by 8PM EST, the team will review what's there, and filter out unnecessary tasks and potential duplicates
    - Assign tasks (can be self assigned)

# Parts 2 & 3
- Research ways to connect Jenkins + GitLab + SonarQube w/docker-compose on startup
    - Would be ideal to not have to manually connect them after `docker-compose up`
    - Connection should be established when applications start up
    - Docker volumes is the likely answer to this problem. See:
        - https://docs.gitlab.com/omnibus/docker/#set-up-the-volumes-location
        - https://hub.docker.com/_/jenkins
        - https://hub.docker.com/_/sonarqube

# Video + Audio Recording
- PowerPoint
- Alternative would be Zoom/Hangouts?
    - This might be better; each teammate could hand off the demo to share their portion
