#!/bin/bash

# Build the image
docker build juice-shop/ -t juiceshop:1.0

# Tag it
docker image tag juiceshop:1.0 localhost:5000/jhu-final-project/juiceshop:1.0

# Push it up to the local registry
docker image push localhost:5000/jhu-final-project/juiceshop:1.0
