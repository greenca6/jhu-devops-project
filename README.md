# JHU Final Project
# Team 3: Craig Green, Kaylin Jarriel, Nick VanRensselaer, Sukhpreet Kaur

This repository holds all of the configuration / source code for the JHU 605.609 Final Project.

It includes Gitlab, Jenkins, and Sonarqube.

# Getting Started

To get started, ensure you have [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) installed.

After cloning the repo, run the following:

```bash
docker-compose up --build
```

This starts up all of the applications (startup may take a few minutes).

> Note: if you're using Windows, you must have given Docker [file sharing](https://docs.docker.com/docker-for-windows/#resources) permissions.

> Note 2: if no changes have been made, you do not need to rebuild with the --build flag


For statuses and runtime information on all of the running services, execute:
```bash
docker-compose ps
```

Below are the locations of where each of the services are running:

1. GitLab -> http://localhost:80
1. Jenkins -> http://localhost:8080
1. SonarQube -> http://localhost:9000

## Logging In To GitLab

Open your browser to http://localhost:80. You should be taken immediately to the login page. The username/password is `root`/`password`.

## Logging In To Jenkins

Open your browser to http://localhost:8080. The username/password is `admin`/`6ee1b3c9ac5f44ed9241baddcd32e7d3`

## Logging In To SonarQube

Open your browser to http://localhost:9000 and navigate to the login page. The username/password is `admin`/`admin`.


# Potential Issues on Different Machines

Depending on your machine, some hiccups may occur. Some examples are:

1. If you have installed NodeJS for Jenkins in the past, Jenkins may skip that part of the setup. However the setup should be executed in order to install Snyk. If Snyk is not installed, there will be an error when running the first Snyk command of the pipeline. If this happens, try updating the NodeJS Installation version from 12.13.0 to 12.13.1.
1. We included juice-shop as a submodule of our Git project. In order to pull in the code for juice-shop, run "git submodule update --init juice-shop".
1. Our push-image.sh script was written in Windows. To run the script in a non-Windows environment, run dos2unix on the script prior to its execution. Also, be sure to run the script from the root directory of the Git project - not from within the scripts directory.