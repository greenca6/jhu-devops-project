const http = require('http');
const url = require('url');

const PORT = 8080;
const TARGET_HOST = 'jenkins';
const TARGET_PORT = '8080';

const proxy = http.createServer((req, res) => {

    const request = url.parse(req.url);

    const options = {
        // Outgoing values are hardcoded to always go to Jenkins
        host: TARGET_HOST,
        port: TARGET_PORT,
        path: request.path,
        method: 'GET',
        headers: req.headers,
    };

    console.log(`GET http://${options.host}:${options.port}${options.path}`);

    const backend_req = http.request(options, (backend_res) => {

        res.writeHead(backend_res.statusCode, backend_res.headers);

        backend_res.on('data', (chunk) => {
            res.write(chunk);
        });

        backend_res.on('end', () => {
            res.end();
        });
    });

    req.on('data', (chunk) => {
        backend_req.write(chunk);
    });

    req.on('end', () => {
        backend_req.end();
    });

});

proxy.listen(PORT, () => {
    console.log(`jenkins-proxy listening on ${PORT}`)
});